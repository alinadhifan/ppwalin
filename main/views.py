from django.shortcuts import render

from django.http import HttpResponse


def home(request):
    return render(request, 'main/home.html')
    
def index(request):
    return render(request, 'main/index.html')

def more(request):
    return render(request, 'main/more.html')

from django.urls import path
from . import views

urlpatterns = [
    path('', views.story5),
    path('post', views.post_story5),
    path('delete/<str:nama_matkul>', views.delete_jadwal),
    path('<str:nama_matkul>', views.detail_jadwal),
]

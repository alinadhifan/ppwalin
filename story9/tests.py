from django.test import TestCase, Client, LiveServerTestCase
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from selenium import webdriver
from selenium.webdriver.chrome.options import Options


# Create your tests here.
class LoginPageUnitTest(TestCase):
    def test_redirect_to_login_page(self):
        response = Client().get('/story9/')
        self.assertEqual(response.status_code, 302)

    

    def test_bad_request_api(self):
        response = Client().get('/story9/api/v1/login/')
        self.assertEqual(response.status_code, 400)

        response = Client().get('/story9/api/v1/signup/')
        self.assertEqual(response.status_code, 400)
